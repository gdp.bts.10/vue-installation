import Vue from 'vue';
import App from './App.vue';
import "./assets/main.css";
import router from './router/index.js';
import Vuelidate from 'vuelidate';
import vuelidateErrorExtractor, { templates } from "vuelidate-error-extractor";
import {store} from "./store/store.js";

Vue.config.productionTip = false;
Vue.use(Vuelidate);
Vue.use(vuelidateErrorExtractor, { 
  template: templates.singleErrorExtractor.foundation6, // you can also pass your own custom template
  messages: {
    required: "Field {attribute} is required",
    email: "Field {attribute} is not a proper email address",
    minLength: "Mininum length is 6"
  },// error messages to use
  attributes: { // maps form keys to actual field names
    email: "Email",
    password: "Password",
  }
})


Vue.component('FormWrapper', templates.FormWrapper)

new Vue({
  router,
  Vuelidate,
  vuelidateErrorExtractor,
  store,
  render: h => h(App)
}).$mount('#app')
