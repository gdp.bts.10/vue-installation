import Vue from "vue";
import Vuex from "vuex"

Vue.use(Vuex);

export const progress = {
    state: {
        parentProgressList : [
            {
                id: 7,
                title: 'do something',
                childProgressList: []
            },
            // {
            //     id: 1,
            //     title: 'not an urgent one',
            //     childList: []
            // },
            // {
            //     id: 2,
            //     title: 'another try',
            //     childList: []
            // },{
            //     id: 3,
            //     title: 'learn something',
            //     childList: []
            // },
        ]
    },
    getters: { getProgressList: (state) => state.parentProgressList,  },

    mutations: {
        updateProgressList: (state,payload) => {
            state.parentProgressList = payload;
            },
        addProgressList: (state, title) => {
            state.parentProgressList.push(title)
        },    
        deleteProgressList: (state,title) => {
            state.parentProgressList.splice(state.parentProgressList.indexOf(title), 1)
        }
    },
    
    actions: {
        updateProgressList: ({commit}, payload) => {
            commit("updateProgressList", payload)
        },
        addProgressList: ({ commit }, task) => {  
            commit("addProgressList", task.title);
        },
        deleteProgressList: ({ commit }, task) => {  
            commit("deleteProgressList", task);
        },
    },
}