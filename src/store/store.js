import { todo } from "./todo";
import { progress } from "./progress";
import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

export const store = new Vuex.Store({
  namespaced: true,
  modules: {
    todo,
    progress
  }
});