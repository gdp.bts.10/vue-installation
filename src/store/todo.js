import Vue from "vue";
import Vuex from "vuex"

Vue.use(Vuex);

export const todo = {
    state: {
        parentList : [
            {
                id: 0,
                title: 'do something',
                childList: []
            },
            {
                id: 1,
                title: 'not an urgent one',
                childList: []
            },
            {
                id: 2,
                title: 'another try',
                childList: []
            },{
                id: 3,
                title: 'learn something',
                childList: []
            },
        ]
    },
    getters: { getTodoList: (state) => state.parentList,  },

    mutations: {
        updateList: (state,payload) => {
            state.parentList = payload;
            },
        addList: (state, task) => {
            state.parentList.push({
                id: task.id,
                title: task.title
            })
        },    
        deleteList: (state,title) => {
            state.parentList.splice(state.parentList.indexOf(title), 1)
        },
        
    },
    
    actions: {
        updateList: ({commit}, payload) => {
            commit("updateList", payload)
        },
        addList: ({ commit }, task) => {  
            commit("addList", task);
            task.title = ""
        },
        deleteList: ({ commit }, task) => {  
            commit("deleteList", task);
        },
    },
}