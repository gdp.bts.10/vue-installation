/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        background: {
          50: '#FFF1E6',
          100: '#FFE6d2',
          200: '#FFDBBF',
          300: '#FFD0AB',
          400: '#FFC598',
          500: '#FFBA84',
          600: '#FFAF70',
          700: '#FFA45D',
          800: '#FF9949',
          900: '#FF8E35',
        }
      }
    },
  },
  plugins: [],
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}']
}
